const RP_FR_1 = require("rp-fr-1").default;
const LEDx = require("rp-fr-1").LEDx;

const GPIO_CLK = 22;
const GPIO_SDI = 23;
const GPIO_LA = 24;
const GPIO_OE = 27;

const driver = new RP_FR_1(GPIO_CLK, GPIO_SDI, GPIO_LA, GPIO_OE);

driver.clear()
    then(() => {
        driver.set_led({ I: LEDx.LED9, R: 1, G: 0, B: 0 });
        return driver.write();
    })
    .then(() => { console.log('done'); })
    .catch((e) => { console.log(e); })