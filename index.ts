import SCT2024_RPI from "rp-sct2024";

type CSET = boolean;
const CSET_1 = false; //["B", "R", "G"]
const CSET_2 = true; //["G", "R", "B"]

export enum LEDx {
    LED1 = 29,
    LED2 = 26,
    LED3 = 23,
    LED4 = 20,
    LED5 = 17,
    LED6 = 13,
    LED7 = 1,
    LED8 = 7,
    LED9 = 4,
}

export interface LED {
    I: LEDx;
    R: 0 | 1;
    G: 0 | 1;
    B: 0 | 1;
}

export default class FR1 {

    SCT2024: SCT2024_RPI;
    STATE: Array<0 | 1>;
    CMAP: Map<LEDx, CSET>

    constructor(
        gpio_clk: number,
        gpio_sdi: number,
        gpio_ls: number,
        gpio_oe: number
    ) {
        this.STATE = Array<0 | 1>(32).fill(0);
        this.SCT2024 = new SCT2024_RPI(gpio_clk, gpio_sdi, gpio_ls, gpio_oe);
        this.CMAP = new Map([
            [LEDx.LED1, CSET_1],
            [LEDx.LED2, CSET_1],
            [LEDx.LED3, CSET_2],
            [LEDx.LED4, CSET_1],
            [LEDx.LED5, CSET_2],
            [LEDx.LED6, CSET_1],
            [LEDx.LED7, CSET_2],
            [LEDx.LED8, CSET_2],
            [LEDx.LED9, CSET_2],
        ]);
    }

    set_led(led: LED): void {
        this.STATE[led.I] = this.CMAP.get(led.I) ? led.G : led.B;
        this.STATE[led.I + 1] = led.R;
        this.STATE[led.I + 2] = this.CMAP.get(led.I) ? led.B : led.G;
    }

    set_state(state: Array<0 | 1>): void {
        if (state.length <= 32) {
            this.STATE = state;
        }
    }

    write(): Promise<any> {
        return this.SCT2024.write(this.STATE).then(() => this.SCT2024.latch());
    }

    turn_on(): Promise<any> {
        return this.SCT2024.enable_output();
    }

    turn_off(): Promise<any> {
        return this.SCT2024.disable_output();
    }

    clear(): Promise<any> {
        this.STATE.fill(0);
        return this.write();
    }

    get_state(): Array<0 | 1> {
        return Object.assign([], this.STATE);
    }
}
